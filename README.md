## Name
Frontend Second assignment.

## Description
Single Page Application using the React framework.
User login. Translate texts. Display user info.

## Image
Actual design.
![image-1.png](./image-1.png)
Plan:
![image.png](./image.png)

## Installation
Install NodeJS.
Run npm start from the root directory of the application folder.
Login page is accesible on localhost:3000

## Usage
login > translate something > check previously translated texts
Or use the heroku deploy: https://tbmk-noroff-translate.herokuapp.com/

## Heroku
https://tbmk-noroff-translate.herokuapp.com/

## Maintainers
@tothbt
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
