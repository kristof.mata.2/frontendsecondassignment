import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../store/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 4
}

const LoginForm = () => {
    //hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()
    //local state
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)
    //side effects
    useEffect(() => {
        //console.log("User changed", user)
        if(user !== null){
            navigate("profile")
        }
    }, [user, navigate])
    //event handlers
    const onLoginSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false);
    };

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span>Username is  required</span>
        }
        if (errors.username.type === 'minLength') {
            return <span>Username can't be shorter than 4 characters.</span>
        }
    })()

    return (
        <>
            <div className='login-main'>
                <div className='login-header'>
                    <div className="login-img">
                        <img className="splash-img" src="assets/Splash.svg" alt="splash"/>
                        <img className="robot-img" src='assets/Logo.png' alt="robot"/>
                    </div>
                    <div className="login-text">
                        <h2 className="login-mainText">Lost in Translation</h2>
                        <h3 className="login-helperText">Get started</h3>
                    </div>
                </div>
                <div className='login-box'>
                    <>
                        <form onSubmit={handleSubmit(onLoginSubmit)}>
                            <div className="form-main" >
                                
                                <label htmlFor="username">
                                    <img className="keyboard-icon" src="assets/keyboard.png" alt="keyboard"/>
                                </label>
                                <label className="div-symbol">| </label>
                                <input
                                    className="input-field"
                                    type="text"
                                    placeholder="What's your name?"
                                    {...register("username", usernameConfig)}
                                />
                                
                                <button className="arrow-button" type="submit" disabled={loading}>
                                    <img className="arrow-icon" src="assets/arrow.png" alt="arrow"/>
                                </button>
                            </div>
                        </form>
                        {errorMessage}
                        {loading && <p>Loggin in...</p>}
                        {apiError && <p>{apiError}</p>}
                    </>
                </div>
                
            </div>
        </>
    )
}

export default LoginForm