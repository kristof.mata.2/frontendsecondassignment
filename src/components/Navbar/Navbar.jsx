import { NavLink } from "react-router-dom"
import { useUser } from '../../store/UserContext'

const Navbar = () => {
    const { user } = useUser()
    
    return (
        <nav className="navbar">
            <ul>
                <li className="headline">Lost in Translation</li>
            </ul>
            { user !== null &&
                <ul className="nav-points">
                    <li>
                        <NavLink className="nav-element" to="/translate">Translate</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav-element" to="/profile">Profile</NavLink>
                    </li>
                </ul>
            }
        </nav>
    )
}
export default Navbar