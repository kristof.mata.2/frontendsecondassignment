import { storageDelete, storageSave } from "../../utils/storage"
import { useUser } from "../../store/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translateHistoryClear } from "../../api/translate"

const ProfileActions = () => {
    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if(window.confirm('Are you sure?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleCear = async () =>{
        if(!window.confirm('Are you sure?\nThis can not be undone!')){
            return;
        }

        const [clearError] = await translateHistoryClear(user.id);
        
        if(clearError !== null){
            return;
        };

        const updatedUser = {
            ...user,
            translations: []
        };

        storageSave(STORAGE_KEY_USER,updatedUser);
        setUser(updatedUser);
    }

    return (
        <div className="button-container">
            <button className="profile-button" onClick={ handleCear }>Clear history</button>
            <button className="profile-button" onClick={ handleLogoutClick }>Logout</button>
        </div>
    )
}
export default ProfileActions