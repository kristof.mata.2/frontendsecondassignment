const ProfileHeader = ({username}) => {
    return (
        <div className="profile-welcome">
            <span className="welcome-text">
                Welcome, back { username }
            </span>
            
        </div>
    )
}
export default ProfileHeader