import ProfileTHistoryItem from "./ProfileTHistoryItem"

const ProfileTranslateHistory = ({ translations }) => {
    
    const translationList = translations.map(
        (translation, index) => <ProfileTHistoryItem key={ index + '-' + translation } translation={ translation }/> 
    )
    
    return (
        <div className="trans-history">
            <span className="trans-history-text">Your translation history</span>
            {translationList.length ===0 && <p>You haven't translated anything yet.</p>}
            <ul className="trans-list">{ translationList }</ul>
        </div>
    )
}

export default ProfileTranslateHistory