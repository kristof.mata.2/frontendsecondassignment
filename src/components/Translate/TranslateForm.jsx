import {useForm} from 'react-hook-form';
import Card from 'react-bootstrap/Card';
import { useState } from 'react';

const TranslateForm = ({onTranslate}) => {
    const {
        register,
        handleSubmit,
    } = useForm();

    const [imageSrces, setImageSrces] = useState([]);

    const onSubmit = ({textToTranslate}) => {
        const letters = textToTranslate.split('').map(char => {
            return char;
        });       
        translate(letters);
        console.log(letters);
        onTranslate(textToTranslate);
    }

    const translate = (letters) => {
        const srces = letters.map( (l) => {
            if((/[a-zA-Z]/).test(l)){
                return `img/${l}.png`;
            }
            return null;
            
        });
        setImageSrces(srces);
        console.log(imageSrces);
    }
 
    return (
        <>
            <form onSubmit={ handleSubmit(onSubmit)} >
                <fieldset style={{backgroundcolor: "#FFC75F"}}>
                    <div className="form-main">
                        <label htmlFor='textToTranslate'>
                            <img className="keyboard-icon" src="assets/keyboard.png" alt="keyboard"/>
                        </label>
                        <input type= "text"{ ...register('textToTranslate')} className="input-field" placeholder="Text to translate.." />
                        <button className="arrow-button" type="submit">
                            <img className="arrow-icon" src="assets/arrow.png" alt="arrow"/>
                        </button>
                    </div>
                </fieldset> 
            </form>
            <Card>
                <Card.Body>
                    {imageSrces.length !==0 && imageSrces.map((imageSrc) =>(
                        <img src={ imageSrc } alt='' key={Math.random()}></img>
                    ))}
                </Card.Body>
            </Card>
        </>
    )
}

export default TranslateForm;