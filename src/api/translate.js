import {createHeaders} from ".";

const apiURL = process.env.REACT_APP_API_URL;

export const addTextToTranslate = async (user,textToTranslate) =>{
    try{
        const response = await fetch(`${apiURL}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [ ...user.translations, textToTranslate]
            })
        });

        if(!response.ok){
            throw new Error('Could not update!');
        }

        const result = await response.json();
        return [null, result];
    }catch(error){
        return[ error.message, null];
    }
}   

export const translateHistoryClear = async (userId) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Could not update!');
        }
        const result = await response.json();
        return [null, result];
    } catch (error) {
        return[ error.message, null];
    }
}