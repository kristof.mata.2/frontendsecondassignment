import TranslateForm from "../components/Translate/TranslateForm";
import {addTextToTranslate} from "../api/translate";
import { useUser } from "../store/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";


const Translate = () => {
    
    const { user, setUser } = useUser()


    const handleTranslate = async (textToTranslate) => {
        
        console.log(textToTranslate);
        const [error, updatedUser] = await addTextToTranslate(user, textToTranslate);
        if(error !== null){
            return
        }
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }
    
    return (
        <>
            <section className="translate-main" id="translate-form">
                <TranslateForm onTranslate={handleTranslate} />
            </section>
        </>
    )
}
export default withAuth(Translate)