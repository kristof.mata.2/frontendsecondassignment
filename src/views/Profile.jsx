import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslateHistory from "../components/Profile/ProfileTranslateHistory"
import { useUser } from "../store/UserContext"
import withAuth from "../hoc/withAuth"
/*import { useEffect } from "react"
import { userById } from "../api/user"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"*/


const Profile = () => {
    const { user } = useUser()

    /*useEffect(() => {
        const findUser = async () => {
            const [error, latestUser ] = await userById(user.id)
            if(error === null){
                storageSave(STORAGE_KEY_USER,latestUser)
                setUser(latestUser)
            }
        }

        //findUser()
    },[setUser, user.id])*/

    return (
        <>
            <div className="profile-main">
                <ProfileHeader username={ user.username }/>
                <ProfileActions/>
                <ProfileTranslateHistory translations={ user.translations }/>
            </div>
        </>
    )
}
export default withAuth(Profile)